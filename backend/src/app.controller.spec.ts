import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Neo4jService } from 'nest-neo4j/dist';

describe('AppController', () => {
  let appController: AppController;
  const mockNeo4jService = {};
  const mockAppService = { getEntities: jest.fn(() => true) };
  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AppController],
      providers: [
        {
          provide: AppService,
          useValue: mockAppService,
        },
        {
          provide: Neo4jService,
          useValue: mockNeo4jService,
        },
      ],
    }).compile();

    appController = app.get<AppController>(AppController);
  });

  describe('App Controller', () => {
    it('should call AppService getEntities method', () => {
      expect(appController.getEntities()).toBe(true);
    });
  });
});

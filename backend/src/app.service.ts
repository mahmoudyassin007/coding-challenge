import { Injectable } from '@nestjs/common';
import { Neo4jService } from 'nest-neo4j/dist';
import * as parser from 'parse-neo4j';

@Injectable()
export class AppService {
  constructor(private readonly neo4jService: Neo4jService) {}

  async getEntities(): Promise<any> {
    const res = await this.neo4jService.read(
      `MATCH path = (a:Entity {parent:''})<-[:children*]-(:Entity)
      WITH collect(path) as paths
      CALL apoc.convert.toTree(paths) yield value
      RETURN value;`,
    );
    return parser.parse(res)[0];
  }
}

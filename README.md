# Coding Challenge

This project consists of 3 main folders:
  - backend, a `nestjs` project that serves the API
  - frontend, a `vuejs` project with `d3.js` that serves the UI
  - import, a vanilla `node.js` project that imports from the github gist into a `neo4j` database

A recording of the working project can be seen here:
![](https://media.giphy.com/media/vgOb7KjUpUcvszitWC/source.gif)


## Import Usage

```bash
npm run install
npm run migrate
```

## Backend Usage

```bash
npm run install
npm run start
```

## Frontend Usage

```bash
npm run install
npm run serve
```
Navigate to the network URL displayed

## Todo Improvements
- Use Vuex to handle states instead of multiple emits 
- Use proper .env for configuration
- Responsive SVG
- Better test coverage
- Typescript Vuejs
- Sass 
